// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic','ngCordova']);
 
app.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
});
 
app.controller('MotionController', function($scope, $ionicPlatform, $ionicLoading, $cordovaDeviceMotion, $cordovaGeolocation) {


  var   motionTypeNotMoving = 1,
        motionTypeWalking = 2,
        motionTypeRunning = 3,
        motionTypeDriving = 4,
        minimumSpeed = 0.3,
        walkingSpeed = 1.9,
        runningSpeed = 7.5,
        drivingSpeed = 25.0,
        isShaking = false;

    // watch Acceleration options
    $scope.accelerationOptions = { 
        frequency: 100, // Measure every 100ms
        deviation : 25  // We'll use deviation to determine the shake event, best values in the range between 25 and 30
    };
    $scope.locationOptions = {timeout: 5000, enableHighAccuracy: true},
 
    // Current measurements
    $scope.measurements = {
        x : null,
        y : null,
        z : null,
        timestamp : null
    }
 
    // Previous measurements    
    $scope.previousMeasurements = {
        x : null,
        y : null,
        z : null
    }   

    $scope.currentPosition = {};
    $scope.accelerationError = "";
    $scope.geolocationError = "";

    

    var successAccelerationCallback = function(result) {
        $scope.accelerationError = "";
        // Set current data  
        $scope.measurements.x = result.x;
        $scope.measurements.y = result.y;
        $scope.measurements.z = result.z;
        $scope.measurements.timestamp = result.timestamp;  

        // Detecta shake  
        $scope.detectShake(result); 
     
    };

    var successLocationCallback = function(position) {
        $scope.geolocationError = "";
        $scope.currentPosition= position.coords;
        var heading = 0;
        var speed = 0;
        $scope.motionType = 0;
                  
        if (position.coords.heading != null){
            heading = position.coords.heading;
        }
        if (position.coords.speed != null){
            speed = position.coords.speed;
        }          
        if (speed < minimumSpeed){
            $scope.motionType = motionTypeNotMoving;
        } else if (speed <= walkingSpeed){
            $scope.motionType = ((isShaking) ? motionTypeRunning : motionTypeWalking);
        } else if (speed <= runningSpeed){
            $scope.motionType = ((isShaking) ? motionTypeRunning : motionTypeDriving);
        } else{
            $scope.motionType = motionTypeDriving;
        }
                  
        console.log("head:" + heading + "speed:" + speed + "latitude:" + position.coords.latitude + "longitude:" + position.coords.longitude + "motionType:" + $scope.motionType);
   
        switch($scope.motionType){
          case 2:
            $scope.motionTranslator =  "Caminando";
            break;
          case 3:
            $scope.motionTranslator =  "Corriendo";
            break;
          case 4:
            $scope.motionTranslator =  "Manejando";
            break;
          case 1:
            $scope.motionTranslator =  "No se detecta movimiento";
            break;
        }
     
        var myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude );
             
        var mapOptions = {
            center: myLatlng,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };          
         
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);          
        var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          title: "You're here!"
        }); 
        $scope.map = map;   
        $ionicLoading.hide(); 

    };    
 
    // Start measurements when Cordova device is ready
    $ionicPlatform.ready(function() {

      $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Acquiring location!'
        });

      $cordovaGeolocation.getCurrentPosition($scope.locationOptions).then(successLocationCallback, function(error) {
                console.log('Error: ' + error);
                $ionicLoading.hide();
                $scope.geolocationError = 'Error watching geolocation: ' + error;
            }); 
 
        //Start Watching method
        $scope.startWatching = function () {
     
            // Device motion configuration
            $scope.accelerationWatchID = $cordovaDeviceMotion.watchAcceleration($scope.accelerationOptions);
            $scope.accelerationWatchID.then(null, function(error) {
                console.log('Error: ' + error);
                $scope.accelerationError = 'Error watching acceleration: ' + error;
            },successAccelerationCallback);       

            $scope.locationWatchID = $cordovaGeolocation.watchPosition( $scope.locationOptions);
            $scope.locationWatchID.then(null, function(error) {
                console.log('Error: ' + error);
                $ionicLoading.hide();
                $scope.geolocationError = 'Error watching geolocation: ' + error;
            },successLocationCallback); 
        };      
 
        // Stop watching method
        $scope.stopWatching = function() {  
            $scope.accelerationWatchID.clearWatch();
            $scope.locationWatchID.clearWatch();
        }       
 
        // Detect shake method      
        $scope.detectShake = function(result) { 
 
            //Object to hold measurement difference between current and old data
            var measurementsChange = {};
 
            // Calculate measurement change only if we have two sets of data, current and old
            if ($scope.previousMeasurements.x !== null) {
                measurementsChange.x = Math.abs($scope.previousMeasurements.x, result.x);
                measurementsChange.y = Math.abs($scope.previousMeasurements.y, result.y);
                measurementsChange.z = Math.abs($scope.previousMeasurements.z, result.z);
            }
 
            // If measurement change is bigger then predefined deviation
            if (measurementsChange.x + measurementsChange.y + measurementsChange.z > $scope.accelerationOptions.deviation) {
                $scope.stopWatching();  // Stop watching because it will start triggering like hell
                console.log('Shake detected'); // shake detected
                isShaking = true;
                setTimeout($scope.startWatching(), 1000);  // Again start watching after 1 sex
 
                // Clean previous measurements after succesfull shake detection, so we can do it next time
                $scope.previousMeasurements = { 
                    x: null, 
                    y: null, 
                    z: null
                }               
 
            } else {
                isShaking = false;  
                // On first measurements set it as the previous one
                $scope.previousMeasurements = {
                    x: result.x,
                    y: result.y,
                    z: result.z
                }
            } 
        }       
 
    });
 
    $scope.$on('$ionicView.beforeLeave', function(){
        $scope.accelerationWatchID.clearWatch(); // Turn off motion detection watcher
        $scope.locationWatchID.clearWatch();
    }); 

    $scope.motionTranslator = "Sensores apagados";

  
});

